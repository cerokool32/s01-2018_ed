package co.starmandevs.ed.stack;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class StackTest {

  @Test
  public void isEmpty() {
    // Given
    Stack<Object> stack = new Stack<Object>(1);

    // when
    stack.push(new Object());
    stack.pop();

    // Then
    assertTrue(stack.isEmpty());
  }

  @Test
  public void isFull() {
    // Given
    Stack<Object> stack = new Stack<Object>(1);

    // when
    stack.push(new Object());

    // Then
    assertTrue(stack.isFull());
  }

  @Test
  public void push() {
    // Given
    Object obj = new Object();
    Stack<Object> stack = new Stack<Object>(1);

    // when
    stack.push(obj);

    // Then
    assertNotNull(stack.pop());
  }

  @Test
  public void pop() {
    // Given
    Object obj = new Object();
    Stack<Object> stack = new Stack<Object>(1);

    // when
    stack.push(obj);
    Object ref = stack.pop();

    // Then
    assertEquals(obj, ref);
  }
}
