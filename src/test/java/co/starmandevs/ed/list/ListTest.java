package co.starmandevs.ed.list;

import org.junit.Assert;
import org.junit.Test;

public class ListTest {

  private List<Object> list;

  @Test
  public void addLastTest_emptyQueue() throws Exception {
    // Given
    list = new List<>();
    Object obj = new Object();

    // When
    list.addLast(obj);

    // Then
    Assert.assertEquals(obj, list.getPayload(0));
  }

  @Test
  public void addLastTest_queueWithElements() throws Exception {
    // Given
    list = new List<>();
    Object obj = new Object();

    list.addLast(new Object());

    // When
    list.addLast(obj);

    // Then
    Assert.assertEquals(obj, list.getPayload(1));
  }

  @Test
  public void addFirstTest_emptyQueue() throws Exception {
    // Given
    list = new List<>();
    Object obj = new Object();

    // When
    list.addFirst(obj);

    // Then
    Assert.assertEquals(obj, list.getPayload(0));
  }

  @Test
  public void addFirstTest_queueWithElements() throws Exception {
    // Given
    list = new List<>();
    Object obj = new Object();

    list.addFirst(new Object());

    // When
    list.addFirst(obj);

    // Then
    Assert.assertEquals(obj, list.getPayload(0));
  }

  @Test
  public void addAtPositionTest() throws Exception {
    // Given
    list = new List<>();
    Object obj = new Object();

    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());

    // When
    list.addAtPos(2, obj);

    // Then
    Assert.assertEquals(obj, list.getPayload(2));
  }

  @Test
  public void addByReferenceTest() throws Exception {
    // Given
    list = new List<>();
    Object obj1 = new Object();
    Object obj2 = new Object();

    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addAtPos(2, obj1);

    // When
    list.addByReference(obj1, obj2);

    // Then
    Assert.assertEquals(obj2, list.getPayload(3));
  }

  @Test
  public void existTest() throws Exception {
    // Given
    list = new List<>();
    Object obj1 = new Object();

    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());

    // When
    list.addAtPos(2, obj1);

    // Then
    Assert.assertTrue(list.exist(obj1));
  }

  @Test
  public void existTest_noValue() throws Exception {
    // Given
    list = new List<>();
    Object obj1 = new Object();

    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());

    // When
    list.addAtPos(2, obj1);

    // Then
    Assert.assertFalse(list.exist(new Object()));
  }

  @Test
  public void getPayloadTest() throws Exception {
    // Given
    list = new List<>();
    Object obj1 = new Object();

    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());

    // When
    list.addAtPos(2, obj1);

    // Then
    Assert.assertEquals(new Integer(2), list.getPosition(obj1));
  }

  @Test(expected = Exception.class)
  public void getPayloadTest_noValue() throws Exception {
    // Given
    list = new List<>();
    Object obj1 = new Object();

    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addAtPos(2, obj1);

    // When
    list.getPosition(new Object());
  }

  @Test
  public void editByReferenceTest() throws Exception {
    // Given
    list = new List<>();
    Object obj1 = new Object();
    Object obj2 = new Object();

    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addAtPos(2, obj1);

    // When
    list.editByReference(obj1, obj2);

    // Then
    Assert.assertEquals(obj2, list.getPayload(2));
  }

  @Test(expected = Exception.class)
  public void editByReferenceTest_noValue() throws Exception {
    // Given
    list = new List<>();
    Object obj1 = new Object();
    Object obj2 = new Object();

    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addAtPos(2, obj1);

    // When
    list.editByReference(new Object(), new Object());
  }

  @Test
  public void editByPositionTest() throws Exception {
    // Given
    list = new List<>();
    Object obj1 = new Object();
    Object obj2 = new Object();

    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addAtPos(2, obj1);

    // When
    list.editByPosition(2, obj2);

    // Then
    Assert.assertEquals(obj2, list.getPayload(2));
  }

  @Test(expected = Exception.class)
  public void editByPositionTest_noPosition() throws Exception {
    // Given
    list = new List<>();
    Object obj1 = new Object();
    Object obj2 = new Object();

    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addAtPos(2, obj1);

    // When
    list.editByPosition(20, obj2);
  }

  @Test
  public void removeByReferenceTest() throws Exception {
    // Given
    list = new List<>();
    Object obj1 = new Object();

    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addAtPos(2, obj1);

    // When
    list.removeByReference(obj1);

    // Then
    Assert.assertFalse(list.exist(obj1));
  }

  @Test(expected = Exception.class)
  public void removeByReferenceTest_noReference() throws Exception {
    // Given
    list = new List<>();
    Object obj1 = new Object();

    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addAtPos(2, obj1);

    // When
    list.removeByReference(new Object());
  }

  @Test
  public void removeByPositionTest() throws Exception {
    // Given
    list = new List<>();
    Object obj1 = new Object();

    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addAtPos(2, obj1);

    // When
    list.removeByPosition(2);

    // Then
    Assert.assertFalse(list.exist(obj1));
  }

  @Test(expected = Exception.class)
  public void removeByPositionTest_noPosition() throws Exception {
    // Given
    list = new List<>();
    Object obj1 = new Object();

    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addFirst(new Object());
    list.addAtPos(2, obj1);

    // When
    list.removeByPosition(20);
  }

  @Test
  public void deleteTest() {
    // Given
    list = new List<>();
    list.addFirst(new Object());

    // When
    list.delete();

    // Then
    Assert.assertEquals(new Integer(0), list.getSize());
  }
}
