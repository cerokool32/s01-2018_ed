package co.starmandevs.ed.queue;

import org.junit.Test;

public class QueueMathTestTest {

  private QueueMathTest<Object> queueMathTest;

  @Test
  public void testMathEquation() {
    // Given
    queueMathTest = new QueueMathTest<Object>(100);

    // When
    for (int i = 0; i < 100; i++) {
      queueMathTest.enqueue(new Object());
    }
    for (int i = 0; i < 50; i++) {
      queueMathTest.dequeue();
    }
    for (int i = 0; i < 50; i++) {
      queueMathTest.enqueue(new Object());
    }
    for (int i = 0; i < 100; i++) {
      queueMathTest.dequeue();
    }
  }
}
