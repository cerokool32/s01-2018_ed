package co.starmandevs.ed.queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class QueueTest {

  private Queue<Object> queue;

  @Test
  public void testEnqueue() {
    // Given
    queue = new Queue<Object>(1);

    // When
    queue.enqueue(new Object());

    // Then
    assertNotNull(queue.dequeue());
  }

  @Test(expected = IllegalStateException.class)
  public void testQueueIsFull() {
    // Given
    queue = new Queue<Object>(1);

    // When
    queue.enqueue(new Object());
    queue.enqueue(new Object());
  }

  @Test
  public void testIsEmpty() {
    // Given
    queue = new Queue<Object>(2);

    // When
    queue.enqueue(new Object());
    queue.enqueue(new Object());
    queue.dequeue();
    queue.dequeue();

    // Then
    assertTrue(queue.isEmpty());
  }

  @Test(expected = IllegalStateException.class)
  public void testDequeueWhenIsEmpty() {
    // Given
    queue = new Queue<Object>(2);

    // When
    queue.enqueue(new Object());
    queue.enqueue(new Object());
    queue.dequeue();
    queue.dequeue();
    queue.dequeue();
  }

  @Test
  public void testIsFull() {
    // Given
    queue = new Queue<Object>(2);

    // When
    queue.enqueue(new Object());
    queue.enqueue(new Object());

    // Then
    assertTrue(queue.isFull());
  }

  @Test
  public void testQueueOrder() {
    // Given
    queue = new Queue<Object>(2);
    Object object1 = new Object();
    Object object2 = new Object();

    // When
    queue.enqueue(object1);
    queue.enqueue(object2);

    // Then
    assertEquals(object1, queue.dequeue());
    assertEquals(object2, queue.dequeue());
  }

  @Test
  public void testNoCircularButRedundant() {
    // Given
    queue = new Queue<Object>(2);

    // When
    queue.enqueue(new Object());
    queue.enqueue(new Object());
    queue.dequeue();
    queue.enqueue(new Object());
    queue.dequeue();
    queue.dequeue();

    // Then
    assertTrue(queue.isEmpty());
  }
}
