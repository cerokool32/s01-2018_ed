package co.starmandevs.ed.tree;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class BinaryTreeTest {

  private BinaryTree<Integer> tree;

  @Test
  public void givenABinaryTree_WhenAddingElements_ThenTreeNotEmpty() {
    // Given
    BinaryTree bt = createBinaryTree();

    // Then
    assertTrue(!bt.isEmpty());
  }

  @Test
  public void givenABinaryTree_WhenAddingElements_ThenTreeContainsThoseElements() {
    // Given
    BinaryTree bt = createBinaryTree();

    // Then
    assertTrue(bt.containsNode(6));
    assertTrue(bt.containsNode(4));

    assertFalse(bt.containsNode(1));
  }

  @Test
  public void givenABinaryTree_WhenAddingExistingElement_ThenElementIsNotAdded() {
    // Given
    BinaryTree bt = createBinaryTree();

    // When
    int initialSize = bt.getSize();
    assertTrue(bt.containsNode(3));

    // Then
    bt.add(3);
    assertEquals(initialSize, bt.getSize());
  }

  @Test
  public void givenABinaryTree_WhenLookingForNonExistingElement_ThenReturnsFalse() {
    // Given
    BinaryTree bt = createBinaryTree();

    // Then
    assertFalse(bt.containsNode(99));
  }

  @Test
  public void givenABinaryTree_WhenDeletingElements_ThenTreeDoesNotContainThoseElements() {
    // Given
    BinaryTree bt = createBinaryTree();

    // When
    assertTrue(bt.containsNode(9));
    bt.delete(9);

    // Then
    assertFalse(bt.containsNode(9));
  }

  @Test
  public void givenABinaryTree_WhenDeletingNonExistingElement_ThenTreeDoesNotDelete() {
    // Given
    BinaryTree bt = createBinaryTree();
    int initialSize = bt.getSize();

    // When
    assertFalse(bt.containsNode(99));
    bt.delete(99);

    // Then
    assertFalse(bt.containsNode(99));
    assertEquals(initialSize, bt.getSize());
  }

  @Test
  public void it_deletes_the_root() {
    // Given
    BinaryTree bt = new BinaryTree();
    int value = 12;

    // When
    bt.add(value);

    // Then
    assertTrue(bt.containsNode(value));
    bt.delete(value);
    assertFalse(bt.containsNode(value));
  }

  @Test
  public void givenABinaryTree_WhenTraversingInOrder_ThenPrintValues() {
    // Given
    BinaryTree bt = createBinaryTree();

    // When
    String response = bt.traverseInOrder();

    // Then
    assertEquals("3 - 4 - 5 - 6 - 7 - 8 - 9 -", response.trim());
  }

  @Test
  public void givenABinaryTree_WhenTraversingPreOrder_ThenPrintValues() {
    // Given
    BinaryTree bt = createBinaryTree();

    // When
    String response = bt.traversePreOrder();

    // Then
    assertEquals("6 - 4 - 3 - 5 - 8 - 7 - 9 -", response.trim());
  }

  @Test
  public void givenABinaryTree_WhenTraversingPostOrder_ThenPrintValues() {
    // Given
    BinaryTree bt = createBinaryTree();

    // When
    String response = bt.traversePostOrder();

    // Then
    assertEquals("3 - 5 - 4 - 7 - 9 - 8 - 6 -", response.trim());
  }

  @Test
  public void givenABinaryTree_WhenTraversingLevelOrder_ThenPrintValues() {
    // Given
    BinaryTree bt = createBinaryTree();

    // When
    String response = bt.traverseLevelOrder();

    // Then
    assertEquals("6 - 4 - 8 - 3 - 5 - 7 - 9 -", response.trim());
  }

  private BinaryTree createBinaryTree() {
    BinaryTree bt = new BinaryTree();

    bt.add(6);
    bt.add(4);
    bt.add(8);
    bt.add(3);
    bt.add(5);
    bt.add(7);
    bt.add(9);

    return bt;
  }
}
