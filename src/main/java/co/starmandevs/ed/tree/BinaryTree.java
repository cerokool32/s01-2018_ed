package co.starmandevs.ed.tree;

import java.util.LinkedList;
import java.util.Queue;
import lombok.Getter;

public class BinaryTree<T extends Comparable> {

  @Getter private Node<T> root;

  public BinaryTree() {
    root = null;
  }

  public void add(T payload) {
    root = addRecursive(root, payload);
  }

  private Node addRecursive(Node current, T payload) {

    if (current == null) {
      return new Node(payload);
    }

    if (payload.compareTo(current.getPayload()) < 0) {
      current.setLeft(addRecursive(current.getLeft(), payload));
    } else if (payload.compareTo(current.getPayload()) > 0) {
      current.setRight(addRecursive(current.getRight(), payload));
    }

    return current;
  }

  public boolean isEmpty() {
    return root == null;
  }

  public int getSize() {
    return getSizeRecursive(root);
  }

  private int getSizeRecursive(Node current) {
    return current == null
        ? 0
        : getSizeRecursive(current.getLeft()) + 1 + getSizeRecursive(current.getRight());
  }

  public boolean containsNode(T payload) {
    return containsNodeRecursive(root, payload);
  }

  private boolean containsNodeRecursive(Node current, T payload) {
    if (current == null) {
      return false;
    }

    if (payload.equals(current.getPayload())) {
      return true;
    }

    return payload.compareTo(current.getPayload()) < 0
        ? containsNodeRecursive(current.getLeft(), payload)
        : containsNodeRecursive(current.getRight(), payload);
  }

  public void delete(T payload) {
    root = deleteRecursive(root, payload);
  }

  private Node deleteRecursive(Node current, T payload) {
    if (current == null) {
      return null;
    }

    if (payload.equals(current.getPayload())) {
      // Case 1: no children
      if (current.getLeft() == null && current.getRight() == null) {
        return null;
      }

      // Case 2: only 1 child
      if (current.getRight() == null) {
        return current.getLeft();
      }

      if (current.getLeft() == null) {
        return current.getRight();
      }

      // Case 3: 2 children
      T smallestValue = findSmallestValue(current.getRight());
      current.setPayload(smallestValue);
      current.setRight(deleteRecursive(current.getRight(), smallestValue));
      return current;
    }
    if (payload.compareTo(current.getPayload()) < 0) {
      current.setLeft(deleteRecursive(current.getLeft(), payload));
      return current;
    }

    current.setRight(deleteRecursive(current.getRight(), payload));
    return current;
  }

  private T findSmallestValue(Node root) {
    return root.getLeft() == null ? (T) root.getPayload() : findSmallestValue(root.getLeft());
  }

  public String traverseInOrder() {
    StringBuilder builder = new StringBuilder();
    traverseInOrder(root, builder);
    return builder.toString();
  }

  private void traverseInOrder(Node node, StringBuilder builder) {
    if (node != null) {
      traverseInOrder(node.getLeft(), builder);
      if (node.equals(root)) {
        builder.append("*" + node.getPayload() + "*");
      } else {
        builder.append(node.getPayload());
      }
      builder.append(" - ");
      traverseInOrder(node.getRight(), builder);
    }
  }

  public String traversePreOrder() {
    StringBuilder builder = new StringBuilder();
    traversePreOrder(root, builder);
    return builder.toString();
  }

  private void traversePreOrder(Node node, StringBuilder builder) {
    if (node != null) {
      builder.append(node.getPayload());
      builder.append(" - ");
      traversePreOrder(node.getLeft(), builder);
      traversePreOrder(node.getRight(), builder);
    }
  }

  public String traversePostOrder() {
    StringBuilder builder = new StringBuilder();
    traversePostOrder(root, builder);
    return builder.toString();
  }

  private void traversePostOrder(Node node, StringBuilder builder) {
    if (node != null) {
      traversePostOrder(node.getLeft(), builder);
      traversePostOrder(node.getRight(), builder);
      builder.append(node.getPayload());
      builder.append(" - ");
    }
  }

  public String traverseLevelOrder() {
    StringBuilder builder = new StringBuilder();
    traverseLevelOrder(builder);
    return builder.toString();
  }

  private void traverseLevelOrder(StringBuilder builder) {
    if (root == null) {
      return;
    }

    Queue<Node> nodes = new LinkedList();
    nodes.add(root);

    while (!nodes.isEmpty()) {

      Node node = nodes.remove();

      builder.append(node.getPayload());
      builder.append(" - ");

      if (node.getLeft() != null) {
        nodes.add(node.getLeft());
      }

      if (node.getLeft() != null) {
        nodes.add(node.getRight());
      }
    }
  }
}
