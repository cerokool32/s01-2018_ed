package co.starmandevs.ed.tree;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Node<T extends Comparable> {

  @NonNull private T payload;
  private Node<T> left;
  private Node<T> right;
}
