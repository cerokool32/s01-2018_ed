package co.starmandevs.ed.list;

import java.util.Optional;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(exclude = "next")
@ToString(exclude = "next")
public class Node<T> {

  private T payload;
  private Optional<Node<T>> next = Optional.empty();
}
