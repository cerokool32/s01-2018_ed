package co.starmandevs.ed.list;

import java.util.Optional;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Slf4j
public class List<T> {

  private Optional<Node<T>> head = Optional.empty();

  @Getter private Integer size = 0;

  public void addLast(T payload) {
    Node<T> newNode = new Node<>();
    newNode.setPayload(payload);

    if (isEmpty()) {
      head = Optional.of(newNode);
    } else {
      Node<T> aux = head.get();

      while (aux.getNext().isPresent()) {
        aux = aux.getNext().get();
      }

      aux.setNext(Optional.of(newNode));
    }
    size++;
  }

  public Boolean isEmpty() {
    return !head.isPresent();
  }

  public void addFirst(T payload) {
    Node<T> newNode = new Node<>();
    newNode.setPayload(payload);

    if (isEmpty()) {
      head = Optional.of(newNode);
    } else {
      newNode.setNext(head);
      head = Optional.of(newNode);
    }

    size++;
  }

  public void addByReference(T reference, T payload) {
    Node<T> newNode = new Node<>();
    newNode.setPayload(payload);

    if (!isEmpty() && exist(reference)) {
      Node<T> aux = head.get();

      while (aux.getPayload() != reference) {
        aux = aux.getNext().get();
      }

      Node<T> next = aux.getNext().get();
      aux.setNext(Optional.of(newNode));
      newNode.setNext(Optional.of(next));

      size++;
    }
  }

  public Boolean exist(T reference) {
    Optional<Node<T>> aux = head;
    Boolean found = Boolean.FALSE;

    while (aux.isPresent() && !found) {
      if (reference == aux.get().getPayload()) {
        found = Boolean.TRUE;
      } else {
        aux = aux.get().getNext();
      }
    }

    return found;
  }

  public void addAtPos(Integer position, T payload) {
    if (position >= 0 && position <= size) {
      Node<T> newNode = new Node<>();
      newNode.setPayload(payload);

      if (position == 0) {
        newNode.setNext(head);
        head = Optional.of(newNode);
      } else {
        if (position == size) {
          Node<T> aux = head.get();

          while (aux.getNext().isPresent()) {
            aux = aux.getNext().get();
          }
          aux.setNext(Optional.of(newNode));
        } else {
          Node<T> aux = head.get();

          for (Integer i = 0; i < (position - 1); i++) {
            aux = aux.getNext().get();
          }

          Node<T> next = aux.getNext().get();
          aux.setNext(Optional.of(newNode));
          newNode.setNext(Optional.of(next));
        }
      }
      size++;
    }
  }

  public T getPayload(Integer position) throws Exception {
    if (position >= 0 && position < size) {

      if (position == 0) {
        return head.get().getPayload();
      } else {
        Node<T> aux = head.get();

        for (Integer i = 0; i < position; i++) {
          aux = aux.getNext().get();
        }

        return aux.getPayload();
      }
    } else {
      throw new Exception("Posicion inexistente en la lista.");
    }
  }

  public Integer getPosition(T reference) throws Exception {
    if (exist(reference)) {
      Node<T> aux = head.get();
      Integer cont = 0;

      while (reference != aux.getPayload()) {
        cont++;
        aux = aux.getNext().get();
      }
      return cont;
    } else {
      throw new Exception("Valor inexistente en la lista.");
    }
  }

  public void editByReference(T reference, T payload) throws Exception {
    if (exist(reference)) {
      Node<T> aux = head.get();

      while (aux.getPayload() != reference) {
        aux = aux.getNext().get();
      }

      aux.setPayload(payload);
    } else {
      throw new Exception("Referencia no existe");
    }
  }

  public void editByPosition(Integer position, T payload) throws Exception {
    if (position >= 0 && position < size) {
      if (position == 0) {
        head.get().setPayload(payload);
      } else {
        Node<T> aux = head.get();
        for (int i = 0; i < position; i++) {
          aux = aux.getNext().get();
        }
        aux.setPayload(payload);
      }
    } else {
      throw new Exception("Posicion no existe");
    }
  }

  public void removeByReference(T reference) throws Exception {
    if (exist(reference)) {
      if (head.get().getPayload() == reference) {
        head = head.get().getNext();
      } else {
        Node<T> aux = head.get();
        while (aux.getNext().get().getPayload() != reference) {
          aux = aux.getNext().get();
        }

        Node<T> next = aux.getNext().get().getNext().get();
        aux.setNext(Optional.of(next));
      }

      size--;
    } else {
      throw new Exception("Referencia no existe");
    }
  }

  public void removeByPosition(Integer position) throws Exception {
    if (position >= 0 && position < size) {
      if (position == 0) {
        head = head.get().getNext();
      } else {
        Node<T> aux = head.get();

        for (Integer i = 0; i < position - 1; i++) {
          aux = aux.getNext().get();
        }

        Node<T> next = aux.getNext().get();
        aux.setNext(next.getNext());
      }

      size--;
    } else {
      throw new Exception("Posicion no existe");
    }
  }

  public void delete() {
    head = Optional.empty();
    size = 0;
  }

  public void print() {
    if (!isEmpty()) {
      Optional<Node<T>> aux = head;
      Integer i = 0;

      while (aux.isPresent()) {
        log.info(i + ". " + aux.toString() + " ->  ");
        aux = aux.get().getNext();
        i++;
      }
    }
  }
}
