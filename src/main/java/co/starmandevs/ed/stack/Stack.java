package co.starmandevs.ed.stack;

public class Stack<T> {

  private int size;
  private int top;
  private T[] stack;

  public Stack(Integer size) {
    this.size = size;
    top = -1;
    stack = (T[]) new Object[size];
  }

  public boolean isEmpty() {
    return top == -1;
  }

  public boolean isFull() {
    return (top + 1) == size;
  }

  public void push(T obj) {
    if (!isFull()) {
      stack[++top] = obj;
    }
  }

  public T pop() {
    if (!isEmpty()) {
      return stack[top--];
    }
    return null;
  }
}
