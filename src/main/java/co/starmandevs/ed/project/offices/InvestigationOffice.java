package co.starmandevs.ed.project.offices;

import co.starmandevs.ed.list.List;
import co.starmandevs.ed.project.domain.Evaluation;
import co.starmandevs.ed.project.domain.Evaluator;
import co.starmandevs.ed.project.domain.Project;
import co.starmandevs.ed.project.domain.ProjectEvaluations;
import co.starmandevs.ed.stack.Stack;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import javax.swing.JOptionPane;

public class InvestigationOffice {

  private List<Evaluator> evaluators;

  private ProjectEvaluations projectEvaluations;

  public InvestigationOffice() {
    evaluators = new List<>();
    projectEvaluations = new ProjectEvaluations();
  }

  public void addEvaluator() {
    String id = JOptionPane.showInputDialog("Ingrese identificación del evaluador");
    String name = JOptionPane.showInputDialog("Ingrese nombre del evaluador");

    if (id != null && name != null && !id.isEmpty() && !name.isEmpty()) {
      Evaluator evaluator = Evaluator.newEvaluator(id, name);
      evaluators.addLast(evaluator);
    } else {
      JOptionPane.showMessageDialog(null, "Creación de evaluador incompleta");
    }
    persistProjects();
  }

  private void persistProjects() {
    File evaluatorsFile = new File(System.getProperty("user.dir") + "evaluators.json");
    File projectsFile = new File(System.getProperty("user.dir") + "projects.json");

    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    String evaluatorsString = "";
    String projectsString = "";

    if (!evaluators.isEmpty()) {
      evaluatorsString = gson.toJson(evaluators);
      try (FileWriter fileWriter = new FileWriter(evaluatorsFile)) {
        fileWriter.write(evaluatorsString);
      } catch (Exception e) {
        // sin manejo
      }
    }
    if (!projectEvaluations.getProjects().isEmpty()) {
      projectsString = gson.toJson(projectEvaluations);
      try (FileWriter fileWriter = new FileWriter(projectsFile)) {
        fileWriter.write(projectsString);
      } catch (Exception e) {
        // sin manejo
      }
    }
  }

  public void retrieveProjectsData() {
    File evaluatorsFile = new File(System.getProperty("user.dir") + "evaluators.json");
    File projectsFile = new File(System.getProperty("user.dir") + "projects.json");

    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    if (evaluatorsFile.exists()) {
      try (FileReader reader = new FileReader(evaluatorsFile)) {
        evaluators = gson.fromJson(reader, evaluators.getClass());
      } catch (Exception e) {
        // sin manejo
      }
    }

    if (projectsFile.exists()) {
      try (FileReader reader = new FileReader(projectsFile)) {
        projectEvaluations = gson.fromJson(reader, projectEvaluations.getClass());
      } catch (Exception e) {
        // sin manejo
      }
    }
  }

  public void addProject() throws Exception {
    Integer projectId = Integer.valueOf(JOptionPane.showInputDialog("Ingrese codigo del proyecto"));
    if (!projectEvaluations.getProjects().isEmpty()) {
      for (int i = 0; i < projectEvaluations.getProjects().getSize(); i++) {
        if (projectEvaluations.getProjects().getPayload(i).getId() == projectId) {
          JOptionPane.showMessageDialog(null, "Proyecto con id " + projectId + " ya existe.");
          return;
        }
      }
    }
    String projectName = JOptionPane.showInputDialog("Ingrese nombre del proyecto");
    Project project = Project.newProject(projectId, projectName);
    projectEvaluations.getProjects().addLast(project);
    persistProjects();
  }

  public void evaluateProject() throws Exception {
    String[] options = new String[evaluators.getSize()];
    for (int i = 0; i < options.length; i++) {
      options[i] = evaluators.getPayload(i).getName();
    }
    int selectedOption =
        JOptionPane.showOptionDialog(
            null, null, "Seleccione evaluador", JOptionPane.PLAIN_MESSAGE, -1, null, options, -1);
    Evaluator selectedEvaluator = evaluators.getPayload(selectedOption);

    options = new String[projectEvaluations.getProjects().getSize()];
    for (int i = 0; i < projectEvaluations.getProjects().getSize(); i++) {
      options[i] = projectEvaluations.getProjects().getPayload(i).getName();
    }
    selectedOption =
        JOptionPane.showOptionDialog(
            null, null, "Seleccione Proyecto", JOptionPane.PLAIN_MESSAGE, -1, null, options, -1);

    Project selectedProject = projectEvaluations.getProjects().getPayload(selectedOption);
    if (evaluatorAlreadyExistInProject(selectedProject, selectedEvaluator)) {
      JOptionPane.showMessageDialog(null, "El proyecto ya fue evaluado por este evaluador");
      return;
    }

    Evaluation evaluation =
        Evaluation.newEvaluation(
            Double.valueOf(Math.random() * 100000).intValue(), selectedEvaluator);

    Integer opt =
        Integer.valueOf(JOptionPane.showInputDialog("Planteamiento del problema y justificación"));
    if (opt >= 0 && opt <= 20) {
      evaluation.setAspect1(opt);
    } else {
      JOptionPane.showMessageDialog(null, "Valor invalido");
      return;
    }

    opt = Integer.valueOf(JOptionPane.showInputDialog("Objetivos"));
    if (opt >= 0 && opt <= 10) {
      evaluation.setAspect2(opt);
    } else {
      JOptionPane.showMessageDialog(null, "Valor invalido");
      return;
    }

    opt = Integer.valueOf(JOptionPane.showInputDialog("Marco de referencia"));
    if (opt >= 0 && opt <= 15) {
      evaluation.setAspect3(opt);
    } else {
      JOptionPane.showMessageDialog(null, "Valor invalido");
      return;
    }

    opt = Integer.valueOf(JOptionPane.showInputDialog("Metodología"));
    if (opt >= 0 && opt <= 20) {
      evaluation.setAspect4(opt);
    } else {
      JOptionPane.showMessageDialog(null, "Valor invalido");
      return;
    }

    opt = Integer.valueOf(JOptionPane.showInputDialog("Pertinencia académica"));
    if (opt >= 0 && opt <= 15) {
      evaluation.setAspect5(opt);
    } else {
      JOptionPane.showMessageDialog(null, "Valor invalido");
      return;
    }

    opt = Integer.valueOf(JOptionPane.showInputDialog("Impacto y pertinencia social"));
    if (opt >= 0 && opt <= 15) {
      evaluation.setAspect6(opt);
    } else {
      JOptionPane.showMessageDialog(null, "Valor invalido");
      return;
    }

    opt = Integer.valueOf(JOptionPane.showInputDialog("Cronograma"));
    if (opt >= 0 && opt <= 5) {
      evaluation.setAspect7(opt);
    } else {
      JOptionPane.showMessageDialog(null, "Valor invalido");
      return;
    }

    String observations = JOptionPane.showInputDialog("Observaciones adicionales");
    evaluation.setObservations(observations);

    selectedProject.getEvaluations().push(evaluation);

    if (selectedProject.getEvaluations().isFull()) {
      projectEvaluations.recalculateResultsTree();
    }

    persistProjects();
  }

  private boolean evaluatorAlreadyExistInProject(Project project, Evaluator evaluator) {
    boolean response = false;
    if (project.getEvaluations().isEmpty()) {
      return response;
    }
    Stack<Evaluation> evaluationStack = project.getEvaluations();
    while (!project.getEvaluations().isEmpty()) {
      Evaluation evaluation = project.getEvaluations().pop();
      if (evaluation.getEvaluator().equals(evaluator)) {
        response = true;
      }
      evaluationStack.push(evaluation);
    }
    project.setEvaluations(evaluationStack);
    return response;
  }

  public String printStats() {
    String response = "No hay datos disponibles";
    if (!projectEvaluations.getResults().isEmpty()) {
      response = projectEvaluations.getResults().traverseInOrder();
    }
    return response;
  }
}
