package co.starmandevs.ed.project.domain;

import co.starmandevs.ed.stack.Stack;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor(staticName = "newProject")
public class Project {

  @NonNull
  private Integer id;

  @NonNull
  private String name;

  private Stack<Evaluation> evaluations = new Stack(3);
}
