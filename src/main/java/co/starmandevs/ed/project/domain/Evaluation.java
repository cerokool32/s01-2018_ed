package co.starmandevs.ed.project.domain;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor(staticName = "newEvaluation")
public class Evaluation {

  @NonNull
  private Integer id;

  @NonNull
  private Evaluator evaluator;

  private Integer aspect1;

  private Integer aspect2;

  private Integer aspect3;

  private Integer aspect4;

  private Integer aspect5;

  private Integer aspect6;

  private Integer aspect7;

  private String observations;
}
