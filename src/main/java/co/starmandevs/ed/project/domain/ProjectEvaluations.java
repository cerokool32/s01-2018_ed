package co.starmandevs.ed.project.domain;

import co.starmandevs.ed.list.List;
import co.starmandevs.ed.stack.Stack;
import co.starmandevs.ed.tree.BinaryTree;
import lombok.Data;

@Data
public class ProjectEvaluations {

  private List<Project> projects = new List<>();

  private BinaryTree<Double> results = new BinaryTree<>();

  public void recalculateResultsTree() throws Exception {
    Double mean = 0d;
    Double total = 0d;
    for (int i = 0; i < projects.getSize(); i++) {
      if (projects.getPayload(i).getEvaluations().isFull()) {
        mean += evaluationSum(projects.getPayload(i));
        total++;
      }
    }
    if (total != 0) {
      mean = mean / total;
      results.add(mean);
    } else {
      return;
    }

    for (int i = 0; i < projects.getSize(); i++) {
      if (projects.getPayload(i).getEvaluations().isFull()) {
        results.add(evaluationSum(projects.getPayload(i)));
      }
    }
  }

  private Double evaluationSum(Project project) {
    Double sum = 0d;
    Stack<Evaluation> evaluationStack = new Stack<>(3);
    while (!project.getEvaluations().isEmpty()) {
      Evaluation evaluation = project.getEvaluations().pop();
      sum +=
          evaluation.getAspect1()
              + evaluation.getAspect2()
              + evaluation.getAspect3()
              + evaluation.getAspect4()
              + evaluation.getAspect5()
              + evaluation.getAspect6()
              + evaluation.getAspect7();
      evaluationStack.push(evaluation);
    }
    project.setEvaluations(evaluationStack);
    return sum;
  }
}
