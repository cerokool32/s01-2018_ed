package co.starmandevs.ed.project.domain;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor(staticName = "newEvaluator")
public class Evaluator {

  @NonNull
  private String id;

  @NonNull
  private String name;
}
