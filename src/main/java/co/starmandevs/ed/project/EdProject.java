package co.starmandevs.ed.project;

import co.starmandevs.ed.project.offices.InvestigationOffice;
import javax.swing.JOptionPane;

public class EdProject {

  public static void main(String... args) throws Exception {
    InvestigationOffice investigationOffice = new InvestigationOffice();
    investigationOffice.retrieveProjectsData();

    boolean keepRunning = true;

    do {
      Integer option =
          Integer.valueOf(
              JOptionPane.showInputDialog(
                  "Menu de opciones:\n"
                      + "\t1. Crear evaluador\n"
                      + "\t2. Crear proyecto\n"
                      + "\t3. Evaluar proyecto\n"
                      + "\t4. Arbol de notas\n"
                      + "\t0. salir"));
      switch (option) {
        case 1:
          investigationOffice.addEvaluator();
          break;
        case 2:
          investigationOffice.addProject();
          break;
        case 3:
          investigationOffice.evaluateProject();
          break;
        case 4:
          JOptionPane.showMessageDialog(null, investigationOffice.printStats());
          break;
        case 0:
          keepRunning = false;
          break;
      }
    } while (keepRunning);
  }
}
