package co.starmandevs.ed.queue;

import java.util.Arrays;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class QueueMathTest<T> {

  private Integer front;
  private Integer rear;

  private Integer size;

  private T[] queue;

  public QueueMathTest(Integer size) {
    this.size = size;
    queue = (T[]) new Object[size];
    front = -1;
    rear = -1;
  }

  public boolean isEmpty() {
    return (front == -1 && rear == -1);
  }

  public boolean isFull() {
    return (rear + 1) % size == front;
  }

  public void enqueue(T value) {
    if (isFull()) {
      throw new IllegalStateException("Queue is full");

    } else if (isEmpty()) {
      front++;
      rear++;
      queue[rear] = value;

    } else {
      rear = (rear + 1) % size;
      queue[rear] = value;
    }
    log.info("rear: {} - front: {} - size: {} - {}", rear, front, size, Arrays.asList(queue));
  }

  public T dequeue() {
    T value = null;
    if (isEmpty()) {
      throw new IllegalStateException("Queue is empty, cant dequeue");
    } else if (front == rear) {
      value = queue[front];
      front = -1;
      rear = -1;

    } else {
      value = queue[front];
      front = (front + 1) % size;
    }
    log.info("rear: {} - front: {} - size: {} - {}", rear, front, size, Arrays.asList(queue));
    return value;
  }
}
